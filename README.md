<h2>👋 Hi I am Nadeem Khan</h2>
I am a web developer with a Bachelor of Technology (B.Tech.) degree in Computer Science & Engineering from AKTU and have 4+ years of web development experience (3+ years of corporate experience in technical profile & 1+ years in freelancing in web development and non-tech profile).

I am experienced in web development and web designing with open source technologies using Magento, PHP, jQuery, AJAX, HTML, CSS, and Photoshop. I have working experience with Model View Controller (MVC).

<h2>👀 Technical Skills</h2> 


<img src="https://img.pngio.com/circle-ecommerce-magento-programming-round-icon-icon-magento-png-512_512.png" width="60" height="60" />  <img src="https://pbs.twimg.com/profile_images/554530862453104641/xq5KwTA1.png" width="60" height="60" />  <img src="https://www.pngkey.com/png/full/550-5509803_js-logo-javascript-logo-circle-png.png" width="60" height="60" />  <img src="https://www.freepnglogos.com/uploads/logo-mysql-png/logo-mysql-mysql-logo-png-images-are-download-crazypng-21.png" width="60" height="60" /><img src="https://cdn.pixabay.com/photo/2017/08/05/11/16/logo-2582748_960_720.png" width="60" height="60" />  <img src="https://cdn.pixabay.com/photo/2017/08/05/11/16/logo-2582747_1280.png" width="60" height="60" />  <img src="https://cdn3.iconfinder.com/data/icons/popular-services-brands/512/github-512.png" width="60" height="60" />  <img src="https://static-00.iconduck.com/assets.00/bitbucket-icon-512x512-exmgeb8n.png" width="60" height="60" />
<img src="https://i.pinimg.com/originals/31/02/38/31023806400284920008d8ebd24a2218.png" width="60" height="60" />  <img src="https://techcrunch.com/wp-content/uploads/2013/08/canva-circle-logo.png?w=730&crop=1" width="60" height="60" />

<h2>📫 How to reach me </h2>

<a href="https://www.linkedin.com/in/nadeem-khan-b1ba18214/"><img src="https://cdn.iconscout.com/icon/free/png-512/linkedin-circle-1868976-1583140.png" width="60" height="60" /></a>  <a href="https://www.instagram.com/inadeem_kassar/"><img src="https://37degreescelsius.net/wp-content/uploads/2019/01/instagram-512.png" width="60" height="60" /></a>  <a href="+919717599422"><img src="https://cdn2.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2-free/128/social-whatsapp-circle-512.png" width="60" height="60" /></a>  <a href="mail:khannadeem243@gmail.com"><img src="https://www.freepnglogos.com/uploads/logo-gmail-png/logo-gmail-png-for-gmail-email-client-mac-app-store-16.png" width="60" height="60" /></a>

<!---
inadeemkhan/inadeemkhan is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
